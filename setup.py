from setuptools import setup

setup(
    name='jobber',
    packages=['jobber'],
    include_package_data=True,
    install_requires=[
        'jobber',
    ],
)